import json
import re
from collections import defaultdict
from itertools import chain
from urllib.parse import urlencode

from loguru import logger
from parsel import Selector
from scrapy.http import FormRequest
from scrapy.downloadermiddlewares.retry import get_retry_request

from shared.gl import definitions_to_pos, singularize
from shared.scrapy import BaseSpider, log_debug_request


def api_query(query, data, **kwargs):
    ns = "com_ideit_ragportal_liferay_dictionary_NormalSearchPortlet"
    query = {
        "p_p_id": ns,
        "p_p_lifecycle": "2",
        "p_p_state": "normal",
        "p_p_mode": "view",
        "p_p_cacheability": "cacheLevelPage",
        **{f"_{ns}_{k}": str(v) for k, v in list(query.items())},
    }
    data = {f"_{ns}_{k}": str(v) for k, v in list(data.items())}
    return FormRequest(
        f"https://academia.gal/dicionario?{urlencode(query)}", formdata=data, **kwargs
    )


def get_better_forms(selector):
    better_forms = selector.xpath(
        ".//*"
        '[@class="References"]'
        '[not(ancestor::span[contains(@class, "Fraseoloxia")])]'
        '[re:test(text(), "Formas?\s+máis\s+recomendables?:")]'
    )
    if (
        better_forms.xpath('ancestor::span[@class="Sense"]')
        and len(
            better_forms.xpath(
                'ancestor::span[@class="Subentry"]//span[@class="Sense"]'
            )
        )
        > 1
    ):
        return set()
    return set(better_forms.xpath(".//@data-noun").getall())


def get_same_as(selector):
    css_classes = ("Subentry", "Sense", "Definition", "Definition__Definition")
    for css_class in css_classes:
        selector = selector.css(f".{css_class}")
        if len(selector) > 1:
            return None
    same_as = selector.css(":scope > a::attr(data-noun)").get()
    if not same_as:
        return None
    return same_as.lower()


def is_covered_elsewhere(selector):
    subentry = selector.css(".Subentry")
    if len(subentry) > 1:
        return False
    return bool(
        subentry.css(":scope > .References")
        and "contracción"
        in (subentry.css(".Subentry__Part_of_speech::text").get() or "")
    )


def parse_invariable(selector):
    invariable_xpath = (
        "//*"
        '[@class="Obs__Texto"]'
        '[./text()[contains(., "invariable")]]'
        '[not(.//ancestor::span[contains(@class, "Fraseoloxia")])]'
    )
    invariable_selector = selector.xpath(invariable_xpath)
    invariable_text = invariable_selector.xpath("text()").get()
    if not invariable_text:
        return False
    match = re.search(
        r"Como\s+(.+?)\s+é\s+invariable\s+en\s+canto\s+ao\s+(número|xénero)",
        invariable_text,
    )
    if match:
        return {match[1]: match[2]}
    match = re.search(r"Como\s+(.+?)\s+é\s+invariable", invariable_text)
    if match:
        return {match[1]: True}
    match = re.search(
        r"Nesta\s+acepción\s+é\s+invariable\s+en\s+plural", invariable_text
    )
    if match:
        subentry = invariable_selector.xpath("ancestor::span[@class='Subentry']")
        po = subentry.css(".Subentry__Part_of_speech::text").get()
        subentries = subentry.xpath(
            f"../span[@class='Subentry'][span[@class='Subentry__Part_of_speech']/text()='{po}']"
        )
        definitions = subentries.css(".Sense")
        definition_count = len(definitions)
        if definition_count == 1:
            return {po: "número"}
        for definition in definitions:
            if not definition.xpath(f".{invariable_xpath}"):
                return "número"
        else:
            return {po: "número"}
    match = re.search(
        r"(?:Nesta|Para\s+esta)\s+acepción\b.*?\bé\s+invariable",
        invariable_text,
    )
    if match:
        subentry = invariable_selector.xpath("ancestor::span[@class='Subentry']")
        po = subentry.css(".Subentry__Part_of_speech::text").get()
        subentries = subentry.xpath(
            f"../span[@class='Subentry'][span[@class='Subentry__Part_of_speech']/text()='{po}']"
        )
        definitions = subentries.css(".Sense")
        definition_count = len(definitions)
        if definition_count == 1:
            return {po: True}
        for definition in definitions:
            if not definition.xpath(f".{invariable_xpath}"):
                return False
        else:
            return {po: True}
    match = re.search(
        r"[Áá]s\s+veces\s+é\s+invariable",
        invariable_text,
    )
    if match:
        return False
    return True


class Spider(BaseSpider):
    name = "drag"

    def entries_request(self, word, direction):
        cmd = f"cmd{direction.capitalize()}TermsSearch"
        return api_query(
            query={"cmd": cmd, "nounTitle": word, "renderMode": "load"},
            data={"noun": word, "homonym": "0"},
            callback=self.parse_entries,
            meta={"browse": direction},
        )

    def entry_request(self, word, **meta):
        return api_query(
            query={"cmd": "cmdNormalSearch", "nounTitle": word, "renderMode": "load"},
            data={"fieldSearchNoun": word},
            callback=self.parse_entry,
            meta={"word": word, **meta},
        )

    def fallback_entry_request(self, word, **meta):
        """Works in some cases where cmdNormalSearch does not work."""
        return api_query(
            query={"cmd": "cmdLoadNoun", "nounTitle": word, "renderMode": "load"},
            data={"noun": word},
            callback=self.parse_entry,
            meta={"word": word, "using_fallback": True, **meta},
        )

    def conjugate_request(self, verb_url, **meta):
        return api_query(
            query={
                "cmd": "cmdConjugateVerb",
                "nounTitle": meta["lemma"],
                "renderMode": "load",
            },
            data={"verb": verb_url},
            callback=self.parse_conjugation,
            meta=meta,
            dont_filter=True,
        )

    def start_requests(self):
        if self.debug_word:
            yield self.entry_request(self.debug_word, browse="none")
            return

        # Request a word that is somewhere in the middle of the dictionary.
        yield self.entry_request("galicismo", browse="both")

        # Request isolated words, which cannot be reached by following the
        # lists of words that go before or after alphabetically. Dates indicate
        # when issues were reported upstream.
        isolated_words = (
            "atuar",
            "berrón",  # 2023-04-08
            "grao",
            "manzanilla",
            "oca",  # 2020-08-30, 2023-04-08
            "receso",
            "sable",
            "solar",  # 2020-08-23, 2023-04-08
        )
        for word in isolated_words:
            yield self.entry_request(word)

    @log_debug_request
    def parse_entry(self, response):
        browse, word = response.meta.get("browse"), response.meta["word"]

        crawl_directions = ("previous", "next")
        if browse in crawl_directions:
            yield self.entries_request(word, browse)
        elif browse == "both":
            for direction in crawl_directions:
                yield self.entries_request(word, direction)

        subentries = {}
        same_as = None
        lemma = None

        data = json.loads(response.text)

        if self.debug_word:
            print(json.dumps(data, indent=4))

        if not response.meta.get("using_fallback", False):
            data = data["items"]
        for index, subentry in enumerate(data, start=1):
            if subentry["title"] != word:
                continue

            if not subentry["htmlContent"]:
                if response.request.meta.get("refresh_cache", False):
                    # Try once more, ensure bypassing the cache.
                    response.request.meta["refresh_cache"] = True
                    new_request = get_retry_request(
                        response.request,
                        spider=self,
                        reason="no_html_content",
                    )
                    yield new_request
                elif not response.meta.get("using_fallback", False):
                    # Do a third try, this time using the fallback method.
                    yield self.fallback_entry_request(word)
                else:
                    logger.error(
                        f"Fallaron todos os intentos de obter os datos de " f"«{word}»."
                    )
                return

            selector = Selector(subentry["htmlContent"])

            if selector.xpath(
                '//*[@class="References"]'
                '[re:test(text(), "Formas?\s+correctas?:")]'
                "//@data-noun"
            ):
                continue

            if is_covered_elsewhere(selector):
                continue

            if not same_as:
                same_as = get_same_as(selector)

            better_forms = get_better_forms(selector)

            pos_css = (
                ".Lemma > .Subentry > .Subentry__Part_of_speech::text, "
                ".Sense__PartOfSpeech::text"
            )
            transposcat_css = ".Transcategorizacion__Categoria::text"
            parts_of_speech = set(
                part_of_speech
                for part_of_speech in chain(
                    selector.css(pos_css).getall(),
                    selector.css(transposcat_css).getall(),
                )
                if part_of_speech.strip()
            )

            lemma, base_feminines = word, None
            if ", " in word:
                # p. ex. «ao, á»
                if not any("locución" in po for po in parts_of_speech):
                    lemma, base_feminines = word.split(", ", maxsplit=1)
                # p. ex. «agacho, ao»
                else:
                    parts = word.split(", ", maxsplit=1)
                    lemma = f"{parts[1]} {parts[0]}"

            # «tamén en singular» en https://academia.gal/dicionario/-/termo/hal%C3%B3xeno
            make_singular = False
            singular_xpath = (
                "//span"
                "[@class='Obs__Texto']"
                "[text()[contains(., 'tamén en singular')]]"
                '[not(.//ancestor::span[contains(@class, "Fraseoloxia")])]'
            )
            for pos_selector in selector.xpath(singular_xpath):
                singular_parent_xpath = (
                    './/ancestor::*[@class="Subentry"]'
                    '//*[@class="Subentry__Part_of_speech"]/text()'
                )
                parent = pos_selector.xpath(singular_parent_xpath).get()
                if parent is None:
                    logger.error(
                        f"No parent found for nested “also in singular” parts "
                        f"of speech reference {pos_selector} of {word} in: "
                        f"{selector.get()}"
                    )
                    continue
                if not parent.endswith(" plural"):
                    logger.error(
                        f"Parent {parent!r} of “also in singular” reference "
                        f"{pos_selector} of {word} does not end in “ plural”, "
                        f"in: {selector.get()}"
                    )
                    continue
                parts_of_speech.remove(parent)
                parts_of_speech.add(parent[: -len(" plural")])
                make_singular = True
            if make_singular:
                lemma = singularize(lemma)

            definitions = selector.xpath(
                '//*[@class="Definition__Definition"]/text()[1]'
            ).getall()
            if any(
                "Individuo desta familia" in definition for definition in definitions
            ) and parts_of_speech == {
                "substantivo masculino plural",
                "substantivo masculino",
            }:
                parts_of_speech = {"substantivo masculino plural", "singular"}

            if not parts_of_speech:
                # Se non se indican as categorías gramaticais, recorrer ás
                # primeiras palabras da definición.
                xpath = '//*[@class="Definition__Definition"]/text()[1]'
                definitions = selector.xpath(xpath).getall()
                parts_of_speech = definitions_to_pos(definitions, word)

            nested_parts_of_speech = defaultdict(set)
            # «Tamén substantivo» en https://academia.gal/dicionario/-/termo/alterador
            transpos_css = ".Example__Transcategorizacion"
            for pos_selector in selector.css(transpos_css):
                transpos_parent_xpath = (
                    './/ancestor::*[@class="Subentry"]'
                    '//*[@class="Subentry__Part_of_speech"]/text()'
                )
                parent = pos_selector.xpath(transpos_parent_xpath).get()
                if parent is None:
                    logger.error(
                        f"No parent found for nested parts of speech "
                        f"{pos_selector} of {word} in: {selector.get()}"
                    )
                for pos in pos_selector.xpath("./text()").re(r"Tamén\s+(.*)"):
                    parts_of_speech.add(pos)
                    if parent is not None:
                        nested_parts_of_speech[parent].add(pos)

            feminine_lemmas = set()
            pos_feminine_lemmas = defaultdict(set)

            main_feminine_lemma = subentry["titleGender"] or base_feminines
            if main_feminine_lemma:
                for feminine_lemma in main_feminine_lemma.split(","):
                    feminine_lemmas.add(feminine_lemma.strip())

            css = ".Lemma__Feminino_irregular::text"
            alternative_feminine_lemma = selector.css(css).get()
            if alternative_feminine_lemma:
                feminine_lemmas.add(alternative_feminine_lemma)

            restriction_xpath = (
                "//*"
                '[@class="Obs__Texto"]'
                '[contains(text(), "só se emprega a forma")]'
            )
            restriction_text = selector.xpath(restriction_xpath).get()
            if restriction_text:
                match = re.search(
                    r"Como\s+(.+?)\s+só\s+se\s+emprega\s+a\s+forma\s+<i>(.*?)</i>",
                    restriction_text,
                )
                if match and match[2] in feminine_lemmas:
                    pos_feminine_lemmas[match[1]].add(match[2])

            verb_url = selector.css(".Lemma__Conxugador ::text").get()

            auxiliar = bool(
                selector.xpath(
                    '//*[@class="Fraseoloxia__Texto"]/text()[contains(., " + inf")]'
                )
            )

            invariable = parse_invariable(selector)
            key = (
                frozenset(better_forms),
                (
                    invariable
                    if not isinstance(invariable, dict)
                    else tuple(sorted(invariable.items()))
                ),
                frozenset(feminine_lemmas),
                verb_url,
            )
            if key in subentries:
                subentry = subentries[key]
                for parent, poses in list(nested_parts_of_speech.items()):
                    subentry["nested_parts_of_speech"][parent] |= poses
                subentry["parts_of_speech"] |= parts_of_speech
            else:
                subentries[key] = {
                    "better_forms": better_forms,
                    "invariable": invariable,
                    "nested_parts_of_speech": nested_parts_of_speech,
                    "parts_of_speech": parts_of_speech,
                    "feminine_lemmas": feminine_lemmas,
                    "pos_feminine_lemmas": {
                        k: sorted(v) for k, v in list(pos_feminine_lemmas.items())
                    },
                    "verb_url": verb_url,
                    "auxiliar": auxiliar,
                }

        subentries = list(subentries.values())
        for subentry in subentries:
            verb_url = subentry.pop("verb_url", None)
            if not verb_url:
                yield {**subentry, "lemma": lemma, "same_as": same_as}
            else:
                yield self.conjugate_request(
                    verb_url, lemma=lemma, same_as=same_as, subentry=subentry
                )

    def parse_entries(self, response):
        browse, entries = response.meta["browse"], json.loads(response.text)
        browse_index = 0 if browse == "previous" else len(entries) - 1
        for index, entry in enumerate(entries):
            meta = {}
            if index == browse_index:
                meta["browse"] = browse
            yield self.entry_request(entry["title"], **meta)

    def parse_conjugation(self, response):
        lemma = response.meta["lemma"]
        same_as = response.meta["same_as"]
        subentry = response.meta["subentry"]
        data = json.loads(response.text)

        if self.debug_word:
            print(json.dumps(data, indent=4))

        html = data["htmlContent"]
        selector = Selector(html)
        tds1 = selector.css(r"[bgcolor='\#F6F3E2'] .anchodos").getall()
        if not tds1:
            # A non-verb with a conjugate button, e.g. «comedeiro».
            yield {**subentry, "lemma": lemma, "same_as": same_as}
            return
        tds2 = selector.css(r"[bgcolor='\#FFFFFF'] .anchodos").getall()

        def _parse_conjugation(conjugation):
            if conjugation.startswith("i_p_"):
                person = int(conjugation[-1])
                if person in (1, 3):
                    tds = tds1
                    index = person - 1
                else:
                    assert person in (4, 6)
                    tds = tds2
                    index = person
            elif conjugation == "s_p_p4":
                tds = selector.css(
                    r".tabsubxuntivo [bgcolor='\#FFFFFF'] .anchocinco"
                ).getall()
                index = 2
            result = "".join(Selector(tds[index]).css("::text").getall())
            return re.sub(r"\s*\[.*", "", result)

        yield {
            **subentry,
            "lemma": lemma,
            "same_as": same_as,
            "conjugation": {
                k: _parse_conjugation(k)
                for k in (
                    "i_p_p1",
                    "i_p_p3",
                    "i_p_p4",
                    "i_p_p6",
                    "s_p_p4",
                )
            },
        }
