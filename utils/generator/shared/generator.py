from importlib import import_module

from loguru import logger
from scrapy.crawler import CrawlerProcess
from scrapy.signals import spider_closed

from shared.processor import Processor
from shared.paths import CACHE_DIR, GENERATOR_ROOT_DIR


class Generator:
    def __init__(self, name):
        self.name = name
        self.jl = CACHE_DIR / f"{name}.jl"
        self.module = import_module(name)
        import_module(f"{name}.spider")
        self._download_finish_reason = "finished"

    def run(self):
        if self._should_download():
            self._download()
        if self._should_process():
            self._process()

    def _should_download(self):
        if not self.jl.exists():
            return True
        if self.jl.stat().st_size > 0:
            logger.info(f"O ficheiro «{self.jl}» xa existe, non se xerará de novo.")
            return False
        else:
            self.jl.unlink()
            return True

    def _download(self):
        process = CrawlerProcess(
            {
                "FEEDS": {
                    str(self.jl): {
                        "format": "jsonlines",
                    },
                },
                "HTTPCACHE_DIR": str(CACHE_DIR / "scrapy"),
            }
        )
        process.crawl(self.module.spider.Spider)

        crawler = next(iter(process.crawlers))
        crawler.signals.connect(self.spider_ended, signal=spider_closed)

        process.start()

    def spider_ended(self, spider, reason):
        self._download_finish_reason = reason

    def _should_process(self):
        if self._download_finish_reason == "finished":
            return True
        logger.error(
            f"A descarga rematou de xeito inesperado, eliminarase o ficheiro "
            f"«{self.jl}» resultante."
        )
        self.jl.unlink()
        return False

    def _process(self):
        kwargs = {
            "jl": self.jl,
            "dic": self.module.dic,
            "fix": GENERATOR_ROOT_DIR / self.name / "fixes.yml",
        }
        Processor(**kwargs).run()
