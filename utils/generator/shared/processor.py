import os
from collections import defaultdict
from copy import deepcopy
from shutil import copyfile
from tempfile import mkstemp

import yaml
from loguru import logger

from shared.builder import Builder, Entry, Status, pluralize
from shared.debug import DEBUG_WORD
from shared.hunspell_gl import conjugate
from shared.jsonlines import iter_json_lines
from shared.gl import (
    INFLEXIBLE_POS,
    MASC_POS,
    VERB_POS,
    singularize,
)
from shared.paths import SRC_DIR


CONJUGATION_FORMS = (
    "i_p_p1",
    "i_p_p3",
    "i_p_p4",
    "i_p_p6",
    "s_p_p4",
)

POS_RENAMINGS = {
    "absoluto": "verbo intransitivo",
    "intransitivo": "verbo intransitivo",
    "participio irregular": "participio",
    "pronominal": "verbo pronominal",
    "transitivo": "verbo transitivo",
}


# Given a lemma with all of the input grammatical categories, those grammatical
# categories are replaced by the specified output grammatical categories.
#
# Order matters.
_POS = ("adxectivo", "indefinido", "substantivo")
POS_DISAMBIGUATIONS = (
    # gender and number → gender
    *(
        ({f"{po} {gender} singular", f"{po} {gender} plural"}, {f"{po} {gender}"})
        for gender in ("feminino", "masculino")
        for po in _POS
    ),
    # gender and number → number
    *(
        ({f"{po} feminino {number}", f"{po} masculino {number}"}, {f"{po} {number}"})
        for number in ("singular", "plural")
        for po in _POS
    ),
    # gender → none
    *(({f"{po} feminino", f"{po} masculino"}, {po}) for po in _POS),
    # number → none
    *(({f"{po} singular", f"{po} plural"}, {po}) for po in _POS),
    # locución → none
    *(
        ({f"locución {po_adj}{sfx}"}, {po})
        for po_adj, po in (
            ("adverbial", "adverbio"),
            ("adxectiva", "adxectivo"),
            ("adxectiva", "adxectivo"),
            ("conxuntiva", "conxunción"),
            ("preposicional", "preposición"),
            ("substantiva", "substantivo"),
            ("substantiva feminina", "substantivo feminino"),
            ("substantiva feminina plural", "substantivo feminino plural"),
            ("substantiva masculina", "substantivo masculino"),
            ("substantiva masculina plural", "substantivo masculino plural"),
            ("adverbial", "adverbio"),
            ("adverbial", "adverbio"),
            ("adverbial", "adverbio"),
        )
        for sfx in ("", " latina")
    ),
    # latina → none
    ({"interxección latina"}, {"interxección"}),
)


def log_jl_debug_word(message, input_path):
    if not DEBUG_WORD:
        return
    for data in iter_json_lines(input_path):
        entry = Entry(**data)
        if entry.lemma != DEBUG_WORD:
            continue
        logger.debug(f"{message}: {entry.json()}")


_EMPTY_FIXES = {
    "feminine": {},
    "invariable": {},
    "parts_of_speech": {},
    "same_as": {},
    "pos_feminine_lemmas": {},
    "entries": {},
    "non_entries": [],
    "conjugation": {},
}


class Processor:
    def __init__(self, *, jl, dic, fix):
        self.input_path = jl
        self.output_path = SRC_DIR / f"{dic}~"
        self._fixes_path = fix
        try:
            with fix.open("rb") as input_file:
                self._fixes = yaml.load(input_file, Loader=yaml.SafeLoader)
        except OSError:
            self._fixes = {}

    def run(self):
        processors = (
            self.remove_non_entries,
            self.remove_neutral_forms,
            self.fix_conjugation,
            self.add_missing_entries,
            self.fix_feminine_lemmas,
            self.fix_invariable,
            self.normalize_parts_of_speech,
            self.fix_parts_of_speech,
            self.fix_same_as,
            self.apply_same_as,
            self.disambiguate_parts_of_speech,
            self.merge_genders,
            self.merge_plurals,
            self.disambiguate_parts_of_speech,
            self.merge_lemmas_by_gender,
            self.merge_lemmas_by_pos,
            self.remove_pos_duplicity,
            self.disambiguate_parts_of_speech,
            self.remove_regular_participles,
            self.fix_pos_feminine_lemmas,
        )
        input_path = self.input_path
        log_jl_debug_word("Raw input", input_path)
        for processor in processors:
            previous_input_path = input_path
            input_path = processor(input_path)
            log_jl_debug_word(processor.__name__, input_path)
            if previous_input_path != self.input_path:
                os.unlink(previous_input_path)
        Builder(self.output_path).build(input_path)
        assert input_path != self.input_path
        os.unlink(input_path)

    def fixes(self, key):
        _fixes = self._fixes.get(key, _EMPTY_FIXES[key])
        reference = f"{self._fixes_path}:{key}"
        return _fixes, reference

    def normalize_parts_of_speech(self, input_path):
        def split_parts_of_speech(parts_of_speech):
            for part_of_speech in parts_of_speech:
                for joiner in ("e", "ou"):
                    if f" {joiner} " in part_of_speech:
                        part_of_speech, part_of_speech_2 = part_of_speech.rsplit(
                            f" {joiner} ",
                            maxsplit=1,
                        )
                        if part_of_speech_2 == "plural":
                            part_of_speech += " plural"
                        else:
                            yield part_of_speech_2
                yield from part_of_speech.split(", ")

        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                parts_of_speech = {
                    POS_RENAMINGS.get(part_of_speech, part_of_speech)
                    for part_of_speech in split_parts_of_speech(entry.parts_of_speech)
                }
                parts_of_speech -= {"participio regular"}
                if "singular" in parts_of_speech:
                    parts_of_speech.remove("singular")
                    found_plurals = False
                    for part in list(parts_of_speech):
                        if part.endswith(" plural"):
                            parts_of_speech.remove(part)
                            parts_of_speech.add(part[:-7])
                            found_plurals = True
                    if found_plurals and entry.lemma.endswith("s"):
                        # p. ex. hamamelidáceas → hamamelidácea
                        entry.singular = singularize(entry.lemma)
                    if not found_plurals and len(parts_of_speech) > 1:
                        logger.error(
                            f"Na palabra {entry.lemma}: Atopouse «singular» como "
                            f"categoría gramatical pero ningunha das outras "
                            f"categorías gramaticais ({parts_of_speech}) son "
                            f"plurais."
                        )
                if "plural" in parts_of_speech:
                    parts_of_speech.remove("plural")
                    found_singulars = False
                    for part in list(parts_of_speech):
                        if part.endswith(" singular"):
                            parts_of_speech.remove(part)
                            parts_of_speech.add(part[:-9])
                            found_singulars = True
                    if not found_singulars and len(parts_of_speech) > 1:
                        logger.error(
                            f"Na palabra {entry.lemma}: Atopouse «plural» como "
                            f"categoría gramatical pero ningunha das outras "
                            f"categorías gramaticais ({parts_of_speech}) son "
                            f"plurais."
                        )
                entry.parts_of_speech = parts_of_speech
                output_file.write(f"{entry.json()}\n")
        return output_path

    def fix_parts_of_speech(self, input_path):
        fixes, reference = self.fixes("parts_of_speech")
        fixes = {
            k: (
                set(v["seen"]),
                set(v["expected"]),
            )
            for k, v in list(fixes.items())
        }
        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.lemma in fixes:
                    seen, expected = fixes[entry.lemma]
                    if entry.parts_of_speech == seen:
                        entry.parts_of_speech = expected
                    elif entry.parts_of_speech == expected:
                        logger.info(
                            f"A fonte corrixiu as categorías gramaticais de "
                            f"{entry.lemma}, pode retirarse o código de "
                            f"{reference} que as corrixe."
                        )
                    else:
                        logger.info(
                            f"A fonte cambiou as categorías gramaticais de "
                            f"{entry.lemma}, pero en vez de ao esperado "
                            f"({expected}) cambiounas a {entry.parts_of_speech}. "
                            f"Cómpre revisar o código de {reference} que as "
                            f"corrixía, por se pode retirarse ou debe modificarse "
                            f"para corrixir as novas categorías gramaticais, e "
                            f"neste último caso cómpre avisar á RAG."
                        )
                output_file.write(f"{entry.json()}\n")
        return output_path

    def disambiguate_parts_of_speech(self, input_path):
        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                for replaced, replacements in POS_DISAMBIGUATIONS:
                    if all(
                        part_of_speech in entry.parts_of_speech
                        for part_of_speech in replaced
                    ) or (
                        any(
                            part_of_speech in entry.parts_of_speech
                            for part_of_speech in replaced
                        )
                        and all(
                            part_of_speech in entry.parts_of_speech
                            and not (
                                entry.invariable
                                if isinstance(entry.invariable, dict)
                                else {}
                            ).get(part_of_speech)
                            for part_of_speech in replacements
                        )
                    ):
                        entry.parts_of_speech -= replaced
                        entry.parts_of_speech |= replacements
                        if entry.pos_feminine_lemmas:
                            for po in list(entry.pos_feminine_lemmas):
                                if po in replaced:
                                    lemma = entry.pos_feminine_lemmas.pop(po)
                                    for replacement in replacements:
                                        entry.pos_feminine_lemmas[replacement] = lemma
                output_file.write(f"{entry.json()}\n")
        return output_path

    def remove_non_entries(self, input_path):
        non_entries, reference = self.fixes("non_entries")
        non_entries = set(non_entries)
        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.lemma in non_entries:
                    continue
                output_file.write(f"{entry.json()}\n")
        return output_path

    def remove_neutral_forms(self, input_path):
        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                entry.feminine_lemmas = [
                    lemma
                    for lemma in entry.feminine_lemmas
                    if not lemma.startswith("neutro:")
                ]
                output_file.write(f"{entry.json()}\n")
        return output_path

    def add_missing_entries(self, input_path):
        entries, reference = self.fixes("entries")
        _, output_path = mkstemp()
        copyfile(input_path, output_path)
        with open(output_path, "a") as output_file:
            for lemma, data in entries.items():
                entry = Entry(
                    lemma,
                    feminine_lemmas=[],
                    pos_feminine_lemmas={},
                    invariable=data.get("invariable", False),
                    parts_of_speech=data["pos"],
                    nested_parts_of_speech={},
                    same_as=None,
                    better_forms=[],
                )
                output_file.write(f"{entry.json()}\n")
        return output_path

    def fix_conjugation(self, input_path):
        fixes, reference = self.fixes("conjugation")
        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.lemma in fixes:
                    expected = fixes[entry.lemma]
                    if entry.conjugation != expected:
                        entry.conjugation = expected
                    else:
                        logger.info(
                            f"A fonte corrixiu a conxugación de {entry.lemma}, "
                            f"pode retirarse o código de {reference} que a "
                            f"corrixe."
                        )
                elif not entry.conjugation and any(
                    "verbo" in po and "latino" not in po for po in entry.parts_of_speech
                ):
                    entry.conjugation = conjugate(entry.lemma)
                    if entry.conjugation is None:
                        logger.warning(
                            f"Non se extraeu a conxugación do verbo "
                            f"«{entry.lemma}» e tampouco foi posíbel "
                            f"aplicarlle unha conxugación regular. É un verbo?"
                        )
                output_file.write(f"{entry.json()}\n")
        return output_path

    def fix_feminine_lemmas(self, input_path):
        fixes, reference = self.fixes("feminine")
        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.lemma in fixes:
                    expected = fixes[entry.lemma]
                    if isinstance(expected, str):
                        expected = [expected]
                    expected = set(expected)
                    if entry.feminine_lemmas != expected:
                        entry.feminine_lemmas = expected
                    else:
                        logger.info(
                            f"A fonte corrixiu os lemas femininos de {entry.lemma}, "
                            f"pode retirarse o código de {reference} que os "
                            f"corrixe."
                        )
                output_file.write(f"{entry.json()}\n")
        return output_path

    def fix_pos_feminine_lemmas(self, input_path):
        fixes, reference = self.fixes("pos_feminine_lemmas")
        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                fix = fixes.get(entry.lemma)
                if fix:
                    for feminine_lemma in list(entry.feminine_lemmas):
                        feminine_fix = fix.get(feminine_lemma)
                        if feminine_fix:
                            entry.feminine_lemmas.remove(feminine_lemma)
                            for pos in feminine_fix:
                                if pos in entry.pos_feminine_lemmas:
                                    logger.info(
                                        f"A fonte corrixiu os lemas femininos de "
                                        f"{entry.lemma}, pode retirarse o código de "
                                        f"{reference} que os corrixe."
                                    )
                                else:
                                    entry.pos_feminine_lemmas[pos] = {feminine_lemma}
                output_file.write(f"{entry.json()}\n")
        return output_path

    def fix_invariable(self, input_path):
        fixes, reference = self.fixes("invariable")
        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.lemma in fixes:
                    expected = fixes[entry.lemma]
                    if entry.invariable != expected:
                        entry.invariable = expected
                    else:
                        logger.info(
                            f"A fonte corrixiu a invariabilidade de {entry.lemma}, "
                            f"pode retirarse o código de {reference} que o "
                            f"corrixe."
                        )
                entry.invariable = entry.invariable or (
                    (
                        (
                            entry.lemma.endswith("as")
                            and entry.lemma != "gas"  # p. ex. alias
                        )
                        or entry.lemma.endswith("os")  # p. ex. menos
                        or (
                            bool(entry.parts_of_speech)
                            and all("latin" in po for po in entry.parts_of_speech)
                        )
                    )
                    and not entry.feminine_lemmas
                    and all("singular" not in po for po in entry.parts_of_speech)
                )
                output_file.write(f"{entry.json()}\n")
        return output_path

    def fix_same_as(self, input_path):
        fixes, reference = self.fixes("same_as")
        fixes = {
            k: (
                v["seen"],
                v["expected"],
            )
            for k, v in list(fixes.items())
        }
        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.lemma in fixes:
                    seen, expected = fixes[entry.lemma]
                    if entry.same_as == seen:
                        entry.same_as = expected
                    elif entry.same_as == expected:
                        logger.info(
                            f"A fonte corrixiu o lemma de referencia de "
                            f"{entry.lemma}, pode retirarse o código de "
                            f"{reference} que o corrixe."
                        )
                    else:
                        logger.info(
                            f"A fonte cambiou o lemma de referencia de "
                            f"{entry.lemma}, pero en vez de ao esperado "
                            f"({expected}) cambiouno a {entry.same_as}. Cómpre "
                            f"revisar o código de {reference} que o corrixía, "
                            f"por se pode retirarse ou debe modificarse "
                            f"para corrixir o novo lema de referencia, e "
                            f"neste último caso cómpre avisar á RAG."
                        )
                output_file.write(f"{entry.json()}\n")
        return output_path

    def apply_same_as(self, input_path):
        fillers = defaultdict(set)
        incomplete = set()
        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.same_as:
                    fillers[entry.same_as].add(entry)
                    key = (
                        entry.lemma,
                        frozenset(entry.feminine_lemmas),
                        frozenset(entry.parts_of_speech),
                    )
                    incomplete.add(key)
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                key = (
                    entry.lemma,
                    frozenset(entry.feminine_lemmas),
                    frozenset(entry.parts_of_speech),
                )
                if key not in incomplete:
                    output_file.write(f"{entry.json()}\n")
                if entry.lemma in fillers:
                    incomplete_entries = fillers.pop(entry.lemma)
                    for incomplete_entry in incomplete_entries:
                        if incomplete_entry.parts_of_speech:
                            for part_of_speech in list(
                                incomplete_entry.parts_of_speech
                            ):
                                if part_of_speech in entry.nested_parts_of_speech:
                                    incomplete_entry.parts_of_speech |= (
                                        entry.nested_parts_of_speech[part_of_speech]
                                    )
                        else:
                            incomplete_entry.parts_of_speech = set(
                                entry.parts_of_speech
                            )
                        if entry.conjugation and incomplete_entry.conjugation:
                            for form in CONJUGATION_FORMS:
                                if entry.conjugation.get(form) == "-":
                                    incomplete_entry.conjugation[form] = "-"
                        output_file.write(f"{incomplete_entry.json()}\n")
        if fillers:
            incomplete_entry_list = "\n".join(
                sorted(
                    f"{entry.lemma} fai referencia a {reference}"
                    for reference, entries in list(fillers.items())
                    for entry in entries
                )
            )
            logger.warning(
                f"Para algunhas entradas do dicionario que facían referencia "
                f"a outras para definicións non se atopou a entrada á que "
                f"facían referencia:\n{incomplete_entry_list}"
            )
        return output_path

    def remove_regular_participles(self, input_path):
        verb_to_participles = defaultdict(set)
        regular_participles = set()
        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                potential_verbs = set()
                if "participio" not in entry.parts_of_speech:
                    continue
                if entry.lemma.endswith("ado"):
                    potential_verbs = {
                        entry.lemma[:-3] + "ar",
                    }
                elif entry.lemma.endswith("ido"):
                    potential_verbs = {
                        entry.lemma[:-3] + "er",
                        entry.lemma[:-3] + "ir",
                    }
                elif entry.lemma.endswith("ído"):
                    potential_verbs = {
                        entry.lemma[:-3] + "ír",
                    }
                for verb in potential_verbs:
                    verb_to_participles[verb].add(entry.lemma)
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if (
                    entry.lemma in verb_to_participles
                    and not entry.parts_of_speech.isdisjoint(VERB_POS)
                ):
                    regular_participles |= verb_to_participles.pop(entry.lemma)
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if (
                    entry.lemma in regular_participles
                    and "participio" in entry.parts_of_speech
                ):
                    entry.parts_of_speech.remove("participio")
                output_file.write(f"{entry.json()}\n")
        return output_path

    def merge_genders(self, input_path):
        def get_potential_roots(word_form):
            if word_form.endswith("unha"):
                return (word_form[:-2],)
            if word_form.endswith("triz"):
                return (
                    word_form[:-4] + "dor",
                    word_form[:-4] + "tor",
                )
            if word_form.endswith("alas"):
                return (word_form[:-4] + "olos",)
            if word_form.endswith("ora"):
                return (
                    word_form[:-1],
                    word_form[:-1] + "o",
                )
            if word_form.endswith("oa"):
                return (
                    word_form[:-1],
                    word_form[:-2] + "ón",
                )
            if word_form.endswith("á"):
                return (word_form[:-1] + "ao",)
            if word_form.endswith("a"):
                return (
                    word_form[:-1] + "o",
                    word_form[:-1] + "e",
                    word_form[:-1] + "ón",
                )
            return tuple()

        potential_roots = defaultdict(list)
        feminines = {}
        roots = defaultdict(list)
        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if not entry.status == Status.RECOMMENDED or not (
                    any("feminino" in po for po in entry.parts_of_speech)
                    and all(
                        (
                            "feminino" in po
                            or po == "contracción"
                            or po in INFLEXIBLE_POS
                            or (
                                entry.invariable
                                if isinstance(entry.invariable, dict)
                                else {}
                            ).get(po)
                        )
                        for po in entry.parts_of_speech
                    )
                ):
                    continue
                for index, potential_root in enumerate(
                    get_potential_roots(entry.lemma)
                ):
                    if DEBUG_WORD in (potential_root, entry.lemma):
                        logger.debug(
                            f"{entry.lemma} ({entry.parts_of_speech}) could "
                            f"be feminine for {potential_root}"
                        )
                    potential_roots[potential_root].append((entry.lemma, index))
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if (
                    entry.lemma in potential_roots
                    and entry.status == Status.RECOMMENDED
                    and not entry.invariable
                    and (
                        entry.feminine_lemmas
                        or all(
                            po in (MASC_POS | INFLEXIBLE_POS)
                            for po in entry.parts_of_speech
                        )
                    )
                ):
                    for feminine, index in potential_roots[entry.lemma]:
                        if feminine not in feminines:
                            feminines[feminine] = (
                                entry.lemma,
                                index,
                                entry.feminine_lemmas,
                            )
                        elif feminine in entry.feminine_lemmas:
                            feminines[feminine] = (
                                entry.lemma,
                                index,
                                entry.feminine_lemmas,
                            )
                        elif feminines[feminine][1] > index:
                            feminines[feminine] = (
                                entry.lemma,
                                index,
                                entry.feminine_lemmas,
                            )
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.lemma in feminines and entry.status == Status.RECOMMENDED:
                    roots[feminines[entry.lemma][0]].append(entry)
                    if DEBUG_WORD == entry.lemma:
                        logger.debug(
                            f"Moved {entry.lemma} ({entry.parts_of_speech}) "
                            f"to root lemma {feminines[entry.lemma][0]}"
                        )
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.lemma in roots and entry.status == Status.RECOMMENDED:
                    for feminine_entry in roots.pop(entry.lemma):
                        inflexible_pos = (
                            feminine_entry.parts_of_speech & INFLEXIBLE_POS
                            | {
                                po
                                for po in (
                                    feminine_entry.invariable
                                    if isinstance(feminine_entry.invariable, dict)
                                    else {}
                                )
                                if (
                                    feminine_entry.invariable
                                    if isinstance(feminine_entry.invariable, dict)
                                    else {}
                                ).get(po)
                            }
                        )
                        if inflexible_pos:
                            inflexible_feminine_entry = deepcopy(feminine_entry)
                            inflexible_feminine_entry.parts_of_speech = inflexible_pos
                            if all("plural" in po for po in inflexible_pos):
                                lemma = inflexible_feminine_entry.lemma
                                if (
                                    all("feminino" in po for po in inflexible_pos)
                                    and entry.feminine_lemmas
                                ):
                                    lemma = next(iter(entry.feminine_lemmas))
                                inflexible_feminine_entry.lemma = pluralize(lemma)
                            output_file.write(f"{inflexible_feminine_entry.json()}\n")
                        flexible_pos = feminine_entry.parts_of_speech - INFLEXIBLE_POS
                        if flexible_pos:
                            if (
                                not entry.feminine_lemmas
                                or feminine_entry.lemma in entry.feminine_lemmas
                            ):
                                entry.feminine_lemmas |= {feminine_entry.lemma}
                                entry.parts_of_speech |= flexible_pos
                            else:
                                output_file.write(f"{feminine_entry.json()}\n")
                if entry.lemma not in feminines:
                    output_file.write(f"{entry.json()}\n")
        return output_path

    def merge_plurals(self, input_path):
        def get_potential_roots(word_form):
            if word_form.endswith("es"):
                return (word_form[:-1].lower(),)
            if word_form.endswith("as") or word_form.endswith("os"):
                return (word_form[:-2].lower() + "o",)
            return tuple()

        potential_roots = defaultdict(list)
        plurals = {}
        roots = defaultdict(list)
        _, output_path = mkstemp()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if not entry.status == Status.RECOMMENDED or not all(
                    (
                        "plural" in po
                        or po == "contracción"
                        or po in INFLEXIBLE_POS
                        or (
                            entry.invariable
                            if isinstance(entry.invariable, dict)
                            else {}
                        ).get(po)
                    )
                    for po in entry.parts_of_speech
                ):
                    continue
                for index, potential_root in enumerate(
                    get_potential_roots(entry.lemma)
                ):
                    if DEBUG_WORD in (potential_root, entry.lemma):
                        logger.debug(
                            f"{entry.lemma} could be plural for {potential_root}"
                        )
                    potential_roots[potential_root].append((entry.lemma, index))
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if (
                    entry.lemma in potential_roots
                    and entry.status == Status.RECOMMENDED
                    and not entry.invariable
                ):
                    for plural, index in potential_roots[entry.lemma]:
                        if plural not in plurals:
                            plurals[plural] = (
                                entry.lemma,
                                index,
                            )
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.lemma in plurals and entry.status == Status.RECOMMENDED:
                    roots[plurals[entry.lemma][0]].append(entry)
                    if DEBUG_WORD == entry.lemma:
                        logger.debug(f"Moved to root lemma {plurals[entry.lemma][0]}")
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.lemma in roots and entry.status == Status.RECOMMENDED:
                    for plural_entry in roots.pop(entry.lemma):
                        inflexible_pos = (
                            plural_entry.parts_of_speech & INFLEXIBLE_POS
                            | {
                                po
                                for po in (
                                    plural_entry.invariable
                                    if isinstance(plural_entry.invariable, dict)
                                    else {}
                                )
                                if (
                                    plural_entry.invariable
                                    if isinstance(plural_entry.invariable, dict)
                                    else {}
                                ).get(po)
                            }
                        )
                        plural_pos = {
                            f"{po} {suffix}"
                            for po in entry.parts_of_speech
                            for suffix in (
                                "plural",
                                "feminino plural",
                                "masculino plural",
                            )
                        }
                        inflexible_pos -= plural_pos
                        if inflexible_pos:
                            inflexible_plural_entry = deepcopy(plural_entry)
                            inflexible_plural_entry.parts_of_speech = inflexible_pos
                            output_file.write(f"{inflexible_plural_entry.json()}\n")
                        flexible_pos = (
                            plural_entry.parts_of_speech - INFLEXIBLE_POS - plural_pos
                        )
                        if flexible_pos:
                            output_file.write(f"{plural_entry.json()}\n")
                if entry.lemma not in plurals:
                    output_file.write(f"{entry.json()}\n")
        return output_path

    def merge_lemmas_by_gender(self, input_path):
        """Xuntar lemas idénticos, de xeito que p. ex. se existe unha entrada de
        substantivo masculino e outra de substantivo feminino, se xuten nun mesmo
        lema."""
        _, output_path = mkstemp()
        seen = set()
        duplicates = set()
        input_entries = defaultdict(list)
        output_entries = {}
        processed = set()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.status != Status.RECOMMENDED:
                    continue
                if entry.lemma in seen:
                    duplicates.add(entry.lemma)
                else:
                    seen.add(entry.lemma)
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.lemma in duplicates:
                    input_entries[entry.lemma].append(entry)
            for lemma, entry_list in input_entries.items():
                shared_entry = None
                other_entries = []
                for entry in entry_list:
                    if not entry.feminine_lemmas and entry.parts_of_speech <= MASC_POS:
                        if not shared_entry:
                            shared_entry = entry
                        else:
                            if all(
                                (
                                    po.replace("masculino", "feminino")
                                    in shared_entry.parts_of_speech
                                )
                                for po in entry.parts_of_speech
                            ):
                                shared_entry.parts_of_speech |= entry.parts_of_speech
                            else:
                                other_entries.append(entry)
                    elif (
                        entry.feminine_lemmas
                        and not entry.parts_of_speech & MASC_POS
                        and (not shared_entry or not shared_entry.feminine_lemmas)
                    ):
                        if not shared_entry:
                            shared_entry = entry
                        else:
                            if all(
                                (
                                    po.replace("masculino", "feminino")
                                    in entry.parts_of_speech
                                )
                                for po in shared_entry.parts_of_speech
                            ):
                                shared_entry.parts_of_speech |= entry.parts_of_speech
                                shared_entry.feminine_lemmas = entry.feminine_lemmas
                            else:
                                other_entries.append(entry)
                    else:
                        other_entries.append(entry)
                output = []
                if shared_entry:
                    output.append(shared_entry)
                if other_entries:
                    output.extend(other_entries)
                output_entries[lemma] = output
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.lemma in processed:
                    continue
                if entry.lemma in output_entries:
                    for replacement in output_entries[entry.lemma]:
                        output_file.write(f"{replacement.json()}\n")
                    processed.add(entry.lemma)
                    continue
                output_file.write(f"{entry.json()}\n")
        return output_path

    def merge_lemmas_by_pos(self, input_path):
        """Xuntar lemas idénticos salvo pola categoría gramatical."""
        _, output_path = mkstemp()
        seen_lemmas = set()
        duplicate_lemmas = set()
        seen_keys = set()
        duplicate_keys = set()
        duplicates = {}
        processed_keys = set()
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.status != Status.RECOMMENDED:
                    continue
                if entry.lemma in seen_lemmas:
                    duplicate_lemmas.add(entry.lemma)
                else:
                    seen_lemmas.add(entry.lemma)
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if (
                    entry.lemma not in duplicate_lemmas
                    or entry.pos_feminine_lemmas
                    or entry.invariable
                    or entry.better_forms
                ):
                    continue
                key = (entry.lemma, frozenset(entry.feminine_lemmas))
                if key in seen_keys:
                    duplicate_keys.add(key)
                else:
                    seen_keys.add(key)
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if (
                    entry.lemma not in duplicate_lemmas
                    or entry.pos_feminine_lemmas
                    or entry.invariable
                    or entry.better_forms
                ):
                    continue
                key = (entry.lemma, frozenset(entry.feminine_lemmas))
                if key not in duplicate_keys:
                    continue
                if key in duplicates:
                    if duplicates[key].conjugation != entry.conjugation:
                        if duplicates[key].conjugation is None:
                            duplicates[key].conjugation = entry.conjugation
                        elif entry.conjugation is None:
                            pass
                        else:
                            if not isinstance(
                                duplicates[key].conjugation["i_p_p1"], dict
                            ):
                                for form in CONJUGATION_FORMS:
                                    duplicates[key].conjugation[form] = {
                                        k: duplicates[key].conjugation[form]
                                        for k in duplicates[key].parts_of_speech
                                        if "verbo" in k
                                    }
                            for k in entry.parts_of_speech:
                                for form in CONJUGATION_FORMS:
                                    if k in duplicates[key].conjugation[form]:
                                        if (
                                            duplicates[key].conjugation[form][k]
                                            == entry.conjugation[form]
                                        ):
                                            continue
                                        if duplicates[key].conjugation[form][k] is None:
                                            duplicates[key].conjugation[form][
                                                k
                                            ] = entry.conjugation[form]
                                            continue
                                        duplicates[key].conjugation[form][k] += (
                                            "/" + entry.conjugation[form]
                                        )
                                    else:
                                        duplicates[key].conjugation[form][
                                            k
                                        ] = entry.conjugation[form]
                    duplicates[key].parts_of_speech |= entry.parts_of_speech
                    duplicates[key].same_as = duplicates[key].same_as or entry.same_as
                    duplicates[key].auxiliar = (
                        duplicates[key].auxiliar or entry.auxiliar
                    )
                else:
                    duplicates[key] = entry
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if not (
                    entry.lemma not in duplicate_lemmas
                    or entry.pos_feminine_lemmas
                    or entry.invariable
                    or entry.better_forms
                ):
                    key = (entry.lemma, frozenset(entry.feminine_lemmas))
                    if key in duplicates:
                        if key in processed_keys:
                            continue
                        entry = duplicates[key]
                        processed_keys.add(key)
                output_file.write(f"{entry.json()}\n")
        return output_path

    def remove_pos_duplicity(self, input_path):
        """Simplificar as categorías gramaticais dunha entrada, ou eliminala
        completamente, se xa van implícitas noutra entrada."""
        _, output_path = mkstemp()
        seen_lemmas = set()
        duplicate_lemmas = set()
        duplicate_pos = defaultdict(set)
        unneeded_pos = defaultdict(set)
        with open(output_path, "w") as output_file:
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.status != Status.RECOMMENDED:
                    continue
                if entry.lemma in seen_lemmas:
                    duplicate_lemmas.add(entry.lemma)
                else:
                    seen_lemmas.add(entry.lemma)
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                if entry.lemma not in duplicate_lemmas:
                    continue
                (entry.lemma, frozenset(entry.feminine_lemmas))
                duplicate_pos[entry.lemma] |= entry.parts_of_speech
            for lemma, pos in duplicate_pos.items():
                if {"substantivo", "substantivo masculino"}.issubset(pos):
                    unneeded_pos[lemma] |= {"substantivo masculino"}
                if {"adxectivo", "adxectivo masculino"}.issubset(pos):
                    unneeded_pos[lemma] |= {"adxectivo masculino"}
            for data in iter_json_lines(input_path):
                entry = Entry(**data)
                entry_unneeded_pos = unneeded_pos.get(entry.lemma)
                if entry_unneeded_pos:
                    entry.parts_of_speech -= entry_unneeded_pos
                output_file.write(f"{entry.json()}\n")
        return output_path
