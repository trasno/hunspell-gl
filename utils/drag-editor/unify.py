#!/usr/bin/env python3

import re
from collections import deque
from pathlib import Path
from os import remove
from shutil import move
from tempfile import TemporaryDirectory

from click import command
from icu import Collator, Locale


@command()
def main():
    build_dir = TemporaryDirectory()
    tmp_filepath = Path(build_dir.name) / "a"
    original_filepath = Path(__file__).parent.parent.parent / "src/rag/gl/correcto.dic"
    generated_filepath = (
        Path(__file__).parent.parent.parent / "src/rag/gl/correcto.dic~"
    )
    collator = Collator.createInstance(Locale("gl_ES.UTF-8"))

    new = set()
    with generated_filepath.open("r") as input_file:
        for line in input_file:
            match = re.search(r"^([^/]+?)ón/10,17 po:substantivo$", line)
            if not match:
                continue
            new.add(match[1])

    old_f = set()
    old_m = set()
    with original_filepath.open("r") as input_file:
        for line in input_file:
            match = re.search(r"^([^/]+?)a/10 po:substantivo_feminino$", line)
            if match:
                if match[1] in new:
                    old_f.add(match[1])
                continue
            match = re.search(r"^([^/]+?)ón/10 po:substantivo_masculino$", line)
            if match:
                if match[1] in new:
                    old_m.add(match[1])

    targets = old_f & old_m
    to_remove = set()
    add = set()
    for index, target in enumerate(sorted(targets), start=1):
        print(f"{target}ón/10, {target}a/10 → {target}ón/10,17 ({index})")
        to_remove.add(f"{target}ón/10 po:substantivo_masculino\n")
        to_remove.add(f"{target}a/10 po:substantivo_feminino\n")
        add.add(f"{target}ón/10,17 po:substantivo\n")
    add = deque(sorted(add, key=collator.getSortKey))
    entry = add.popleft()

    in_match, written = False, False
    pattern = re.compile(r"^([^ ]+) po:substantivo$")
    with tmp_filepath.open("w") as output_file:
        with original_filepath.open("r") as input_file:
            for line in input_file:
                if line in to_remove:
                    to_remove.remove(line)
                    continue
                if written:
                    output_file.write(line)
                elif in_match:
                    if not pattern.match(line) or collator.compare(entry, line) < 0:
                        output_file.write(entry)
                        if add:
                            entry = add.popleft()
                        else:
                            written = True
                    output_file.write(line)
                elif pattern.match(line):
                    in_match = True
                    if collator.compare(entry, line) < 0:
                        output_file.write(entry)
                        if add:
                            entry = add.popleft()
                        else:
                            written = True
                    output_file.write(line)
                else:
                    output_file.write(line)
            if add:
                lines = "\n".join(add)
                print(
                    f"ERROR: Found no place to write for the following lines:\n{lines}"
                )

    remove(original_filepath)
    move(tmp_filepath, original_filepath)


if __name__ == "__main__":
    main()
