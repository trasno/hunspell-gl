#!/usr/bin/env python3

import re
from collections import deque
from pathlib import Path
from os import remove
from shutil import move
from tempfile import TemporaryDirectory

from click import command
from icu import Collator, Locale


@command()
def main():
    build_dir = TemporaryDirectory()
    tmp_filepath = Path(build_dir.name) / "a"
    original_filepath = Path(__file__).parent.parent.parent / "src/rag/gl/correcto.dic"
    generated_filepath = (
        Path(__file__).parent.parent.parent / "src/rag/gl/correcto.dic~"
    )
    collator = Collator.createInstance(Locale("gl_ES.UTF-8"))

    new = set()
    with original_filepath.open("r") as input_file:
        for line in input_file:
            match = re.search(r"^([^ ]+?)/10,15 po:substantivo$", line)
            if not match:
                continue
            new.add(match[1])

    to_add = set()
    to_remove = set()
    index = 1
    with generated_filepath.open("r") as input_file:
        for line in input_file:
            match = re.search(r"^([^ ]+?)/10 po:substantivo_masculino$", line)
            if match:
                if match[1] in new:
                    print(f"{match[1]}: -feminino ({index})")
                    index += 1
                    to_remove.add(f"{match[1]}/10,15 po:substantivo\n")
                    to_add.add(f"{match[1]}/10 po:substantivo_masculino\n")
                continue

    to_add = deque(sorted(to_add, key=collator.getSortKey))
    entry = to_add.popleft()

    in_match, written = False, False
    pattern = re.compile(r"^([^ ]+) po:substantivo_masculino$")
    with tmp_filepath.open("w") as output_file:
        with original_filepath.open("r") as input_file:
            for line in input_file:
                if line in to_remove:
                    to_remove.remove(line)
                    continue
                if written:
                    output_file.write(line)
                elif in_match:
                    if not pattern.match(line) or collator.compare(entry, line) < 0:
                        output_file.write(entry)
                        if to_add:
                            entry = to_add.popleft()
                        else:
                            written = True
                    output_file.write(line)
                elif pattern.match(line):
                    in_match = True
                    if collator.compare(entry, line) < 0:
                        output_file.write(entry)
                        if to_add:
                            entry = to_add.popleft()
                        else:
                            written = True
                    output_file.write(line)
                else:
                    output_file.write(line)
            if to_add:
                lines = "\n".join(to_add)
                print(
                    f"ERROR: Found no place to write for the following lines:\n{lines}"
                )

    remove(original_filepath)
    move(tmp_filepath, original_filepath)


if __name__ == "__main__":
    main()
