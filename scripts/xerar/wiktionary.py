# -*- coding:utf-8 -*-

from mediawiki import MediaWikiGenerator  # Dictionary generators.
from mediawiki import EntryGenerator  # Entry generators.
from mediawiki import PageLoader, CategoryBrowser  # Page generators.
from mediawiki import TitleParser, FirstSentenceParser, LineParser, TableParser  # Page parsers.
from mediawiki import EntryParser  # Page parsers.



class WiktionaryGenerator(MediaWikiGenerator):

    def __init__(self, languageCode, resource, partOfSpeech, entryGenerators):
        super(WiktionaryGenerator, self).__init__(
                siteName="wiktionary",
                languageCode=languageCode,
                resource=resource,
                partOfSpeech=partOfSpeech,
                entryGenerators=entryGenerators,
            )


class GalizionarioGenerator(WiktionaryGenerator):

    def __init__(self, resource, partOfSpeech, entryGenerators):
        super(GalizionarioGenerator, self).__init__(
                "gl",
                resource,
                partOfSpeech,
                entryGenerators=entryGenerators,
            )




class GalizionarioNamesGenerator(GalizionarioGenerator):

    def __init__(self):

        pageNames = ["Apéndice:Nomes de persoa",]
        pageLoader = PageLoader(pageNames=pageNames)

        tableParser = TableParser(cellNumbers=[0, 3, 4])

        super(GalizionarioNamesGenerator, self).__init__(
            resource = "antroponimia.dic",
            partOfSpeech = "antropónimo",
            entryGenerators = [
                EntryGenerator(
                    pageGenerators=[pageLoader,],
                    pageParser=tableParser,
                    entryParser=EntryParser(
                        commaSplitter=True,
                        commaFilter=False,
                        semicolonSplitter=True,
                    )
                ),
            ]
        )



class WiktionaryEnGenerator(WiktionaryGenerator):

    def __init__(self, resource, partOfSpeech, entryGenerators):
        super(WiktionaryEnGenerator, self).__init__(
                "en",
                resource,
                partOfSpeech,
                entryGenerators=entryGenerators,
            )


class WiktionaryEnNamesGenerator(WiktionaryEnGenerator):

    def __init__(self):

        import string

        pageNames = set()
        for pagePrefix in ["Appendix:Female given names/", "Appendix:Male given names/"]:
            for letter in list(string.ascii_uppercase):
                pageNames.add(pagePrefix + letter)
        pageLoader = PageLoader(pageNames=pageNames)

        namePattern = "^: *(\'\'\')? *\[\[ *([^]|]+\|)? *(?P<entry>[^]|]+) *\]\]"
        ignorePattern = "^-"
        lineParser = LineParser(namePattern, ignorePattern=ignorePattern)

        super(WiktionaryEnNamesGenerator, self).__init__(
                "antroponimia.dic",
                "antropónimo",
                [
                    EntryGenerator(
                        pageGenerators=[pageLoader,],
                        pageParser=lineParser,
                    ),
                ]
            )



def loadGeneratorList():

    generators = []


    # Galizionario.

    generators.append(GalizionarioNamesGenerator())

    generators.append(GalizionarioGenerator(
        resource = "toponimia/xeral.dic",
        partOfSpeech = "topónimo",
        entryGenerators= [
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Toponimia en galego"],
                        ignoreCategoryNames = ["Toponimia de Galicia en galego"]
                    ),
                ],
            ),
        ]
    ))

    generators.append(GalizionarioGenerator(
        "toponimia/galicia.dic",
        "topónimo",
        entryGenerators= [
            EntryGenerator(
                pageGenerators=[
                    CategoryBrowser(
                        categoryNames = ["Toponimia de Galicia en galego"]
                    ),
                ]
            )
        ]
    ))


    # Wiktionary en inglés.

    generators.append(WiktionaryEnNamesGenerator())


    # TODO: Desenvolver un sistema de determinación das liñas correctas de feminino e plural para o Hunspell.
    # Por exemplo: «capixaba/10» para «capixaba/s».
    #generators.append(GalizionarioGenerator(
        #resource = u"xentilicio/xeral.dic",
        #partOfSpeech = u"xentilicio",
        #categoryNames = [u"Xentilicio en galego"]
    #))

    return generators
