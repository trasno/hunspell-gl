# -*- coding:utf-8 -*-

from mediawiki import MediaWikiGenerator  # Dictionary generators.
from mediawiki import EntryGenerator  # Entry generators.
from mediawiki import PageLoader, CategoryBrowser  # Page generators.
from mediawiki import TitleParser, FirstSentenceParser, LineParser, TableParser  # Page parsers.
from mediawiki import EntryParser  # Page parsers.


# Wikipedia generator.

class WikipediaGenerator(MediaWikiGenerator):

    def __init__(self, languageCode, resource, partOfSpeech, entryGenerators):
        super(WikipediaGenerator, self).__init__(
                siteName="wikipedia",
                languageCode=languageCode,
                resource=resource,
                partOfSpeech=partOfSpeech,
                entryGenerators=entryGenerators,
            )




# Language-specific Wikipedia generators.

class GalipediaGenerator(WikipediaGenerator):

    def __init__(self, resource, partOfSpeech, entryGenerators):
        super(GalipediaGenerator, self).__init__(
                "gl",
                resource,
                partOfSpeech,
                entryGenerators=entryGenerators,
            )



class WikipediaEnGenerator(WikipediaGenerator):

    def __init__(self, resource, partOfSpeech, entryGenerators):
        super(WikipediaEnGenerator, self).__init__(
                "en",
                resource,
                partOfSpeech,
                entryGenerators=entryGenerators,
            )


class WikipediaEsGenerator(WikipediaGenerator):

    def __init__(self, resource, partOfSpeech, entryGenerators):
        super(WikipediaEsGenerator, self).__init__(
                "es",
                resource,
                partOfSpeech,
                entryGenerators=entryGenerators,
            )


class WikipediaHuGenerator(WikipediaGenerator):

    def __init__(self, resource, partOfSpeech, entryGenerators):
        super(WikipediaHuGenerator, self).__init__(
                "hu",
                resource,
                partOfSpeech,
                entryGenerators=entryGenerators,
            )


class WikipediaPtGenerator(WikipediaGenerator):

    def __init__(self, resource, partOfSpeech, entryGenerators):
        super(WikipediaPtGenerator, self).__init__(
                "pt",
                resource,
                partOfSpeech,
                entryGenerators=entryGenerators,
            )




# Helpers.

class GalipediaLocalidadesGenerator(GalipediaGenerator):

    def __init__(self, countryName, categoryNames = ["Cidades de {name}"], pageParser=None):

        parsedCategoryNames = []
        for categoryName in categoryNames:
            parsedCategoryNames.append(categoryName.format(name=countryName))

        pattern = "(Alcaldes|Arquitectura|Capitais|Comunas|Concellos|Festas?|Imaxes|Igrexa|Galería|Historia|Listas?|Localidades|Lugares|Municipios|Parroquias|Principais cidades) "
        categoryBrowser = CategoryBrowser(
            categoryNames=parsedCategoryNames,
            invalidPagePattern = "(?i)^(Wikipedia:|{pattern}[a-z])".format(pattern=pattern),
            validCategoryPattern = "(?i)^(Antig[ao]s )?(Cidades|Comunas|Concellos|Municipios|Parroquias|Vilas) ",
            invalidCategoryAsPagePattern = "(?i){pattern}[a-z]|.+sen imaxes$".format(pattern=pattern),
        )

        entryParser = EntryParser()
        if countryName == "España":
            entryParser = EntryParser(basqueFilter=True)

        super(GalipediaLocalidadesGenerator, self).__init__(
            resource = "onomástica/toponimia/localidades/{name}.dic".format(name=countryName.lower().replace(" ", "-")),
            partOfSpeech = "topónimo",
            entryGenerators=[
                EntryGenerator(
                    pageGenerators=[categoryBrowser,],
                    pageParser=pageParser,
                    entryParser=entryParser,
                )
            ],
        )



class GalipediaRexionsGenerator(GalipediaGenerator):

    def __init__(self, countryName, categoryNames = ["Rexións de {name}"], pageParser=None):

        parsedCategoryNames = []
        for categoryName in categoryNames:
            parsedCategoryNames.append(categoryName.format(name=countryName))

        categoryPattern = "Áreas municipais|Comarcas|Condados|Departamentos|Distritos|Divisións|Estados|Partidos xudiciais|Periferias|Provincias|Repúblicas|Rexións|Subdivisións|Subrexións"
        categoryBrowser = CategoryBrowser(
            categoryNames=parsedCategoryNames,
            invalidPagePattern = "^((Batalla|Lista|{}) |Comunidade autónoma)".format(categoryPattern),
            validCategoryPattern = "^({}) ".format(categoryPattern),
            invalidCategoryAsPagePattern = "^(Capitais|Categorías|Concellos|Deporte|Gobernos|Nados|Parlamentos|Personalidades|Políticas|Presidentes) ",
        )

        super(GalipediaRexionsGenerator, self).__init__(
            resource = "onomástica/toponimia/rexións/{name}.dic".format(name=countryName.lower().replace(" ", "-")),
            partOfSpeech = "topónimo",
            entryGenerators=[
                EntryGenerator(
                    pageGenerators=[categoryBrowser,],
                    pageParser=pageParser,
                )
            ],
        )



class GalipediaNamesGenerator(GalipediaGenerator):

    def __init__(self):

        pageNames = ["Lista de nomes masculinos en galego", "Lista de nomes femininos en galego"]
        pageLoader = PageLoader(pageNames=pageNames)

        categoryBrowser = CategoryBrowser(
            categoryNames = ["Antroponimia",],
            invalidPagePattern = "^(Antroponimia$|Lista )",
            invalidCategoryPattern = "^Identidade$",
        )

        namePattern = "^\* *\'\'\' *(\[\[)? *([^]|]+\| *)?(?P<entry>[^]|]+) *(\]\])? *\'\'\'"
        lineParser = LineParser(namePattern)

        super(GalipediaNamesGenerator, self).__init__(
            resource = "onomástica/antroponimia/xeral.dic",
            partOfSpeech = "antropónimo",
            entryGenerators = [
                EntryGenerator(
                    pageGenerators=[pageLoader,],
                    pageParser=lineParser,
                ),
                EntryGenerator(
                    pageGenerators=[categoryBrowser,],
                    pageParser=FirstSentenceParser(),
                ),
            ]
        )



def loadGeneratorList():

    generators = []


    # Galipedia

    generators.append(GalipediaGenerator(
        resource = "bioquímica.dic",
        partOfSpeech = "substantivo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Coencimas",],
                        invalidCategoryPattern = "^Personalidades",
                    ),
                ],
                pageParser = FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/administración/intelixencia.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Servizos de intelixencia"],
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/administración/ministerios.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Ministerios de España"],
                        validCategoryPattern = "^Antigos ministerios"
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    pattern = "Grafiteiros|Personalidades|Pintores"
    generators.append(GalipediaGenerator(
        resource = "onomástica/antroponimia/arte/pintura.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Personalidades da pintura"],
                        invalidPagePattern = "^(Imaxes |Listas |Premio |{})".format(pattern),
                        validCategoryPattern = "^({})".format(pattern),
                    ),
                ],
                pageParser = FirstSentenceParser(),
            )
        ],
    ))

    pattern = "Biólogos|Bioquímicos|Científicos|Empresarios|Farmacéuticos|Físicos|Matemáticos|Médicos|Personalidades|Químicos"
    generators.append(GalipediaGenerator(
        resource = "onomástica/antroponimia/ciencia.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Personalidades da ciencia"],
                        invalidPagePattern = "^(Día do Científico|Imaxes |Premio |{})".format(pattern),
                        validCategoryPattern = "^({})".format(pattern),
                    ),
                ],
                pageParser = FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/antroponimia/países/costa-rica.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = [
                            "Presidentes de Costa Rica",
                        ],
                        invalidPagePattern = "^Presidente de Costa Rica$",
                    ),
                ],
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/antroponimia/países/españa.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = [
                            "Escritores de Galicia",
                            "Finados en 1975",
                            "Nados en Pontedeume",
                            "Nados en Salceda de Caselas",
                            "Reis de Galicia",
                            "Vicepresidentes do goberno de España",
                        ],
                        invalidPagePattern = "^(Dinastía|Imaxes de|Lista d|Xeración)",
                        validCategoryPattern = "^(Dinastía|Escritores|Poetas|Tradutores|Reis|Xeración)",
                        invalidCategoryAsPagePattern = "^(Lista d)",
                    ),
                ],
                pageParser = FirstSentenceParser(),
            )
        ],
    ))

    pattern = "Chanceleres|Personalidades|Políticos|Presidente do Consello de Comisarios do Pobo|Presidentes|Primeiros [Mm]inistros|Secretarios|Taoiseach"
    generators.append(GalipediaGenerator(
        resource = "onomástica/antroponimia/política/primeiros-ministros.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Primeiros ministros"],
                        invalidPagePattern = "^(Imaxes |Listas |Premio |{})".format(pattern),
                        validCategoryPattern = "^({})".format(pattern),
                    ),
                ],
                pageParser = FirstSentenceParser(),
            )
        ],
    ))

    pattern = "Actores|Personalidades|Xornalistas"
    generators.append(GalipediaGenerator(
        resource = "onomástica/antroponimia/radio.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Personalidades da radio"],
                        invalidPagePattern = "^(Imaxes |Premio |{})".format(pattern),
                        validCategoryPattern = "^({})".format(pattern),
                    ),
                ],
                pageParser = FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/antroponimia/relixión.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = [
                            "Personaxes bíblicos",
                        ],
                        invalidPagePattern = "^Apóstolo$",
                        validCategoryPattern = "^Apóstolos",
                    ),
                ],
                pageParser = FirstSentenceParser(),
            )
        ],
    ))

    pattern = "Presentadores"
    generators.append(GalipediaGenerator(
        resource = "onomástica/antroponimia/televisión/presentadores.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Presentadores de televisión"],
                        invalidPagePattern = "^(Imaxes |Listas |Premio |{})".format(pattern),
                        validCategoryPattern = "^({})".format(pattern),
                    ),
                ],
                pageParser = FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaNamesGenerator())

    pattern = "(Arquitectura relixiosa|Basílicas|Capelas|Catedrais|Colexiatas|Conventos|Ermidas|Igrexas|Mosteiros|Mosteiros e conventos|Pórticos|Santuarios|Templos) "
    generators.append(GalipediaGenerator(
        resource = "onomástica/arquitectura/relixión.dic",
        partOfSpeech = "nome propio",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Arquitectura relixiosa por países"],
                        invalidPagePattern = "^({pattern}|Galería de imaxes|Iglesias gallegas de la Edad Media$)".format(pattern=pattern),
                        validCategoryPattern = "^{pattern}".format(pattern=pattern),
                        invalidCategoryAsPagePattern = "^(Imaxes) ",
                    ),
                ],
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/arte/escultura/relixión.dic",
        partOfSpeech = "nome propio",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Escultura relixiosa de Galicia"],
                        validCategoryPattern = "^(Baldaquinos d|Cruceiros d)",
                        invalidCategoryAsPagePattern = "^Imaxes d",
                    ),
                ],
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/astronomía/planetas.dic",
        partOfSpeech = "nome propio",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Planetas"],
                        invalidPagePattern = "^(Lista d|Planeta anano$|Planeta($| ))",
                        validCategoryPattern = "^(Candidatos a planeta|Planetas |Plutoides$)",
                        invalidCategoryAsPagePattern = "^Sistemas planetarios$",
                    ),
                ],
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/deporte/acontecementos.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Eventos deportivos"],
                        validCategoryPattern = "^(Acontecementos|Campionatos|Competicións|Ligas|Mundiais|Torneos)",
                        invalidCategoryAsPagePattern = "(^(Balóns|Goleadores|Tempadas)|.* nos Xogos Olímpicos$)",
                        invalidPagePattern = ".*\d+-\d+",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/deporte/fórmula1/escuderías.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Escuderías de Fórmula 1"],
                        validCategoryPattern = "^(Escuderías)",
                        invalidPagePattern = "^Lista",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/economía/feiras.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Institucións feirais",],
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/economía/índices-bolsistas.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Índices bolsistas",],
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/economía/empresas/banca.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Bancos e caixas de aforro"],
                        invalidPagePattern = "^(Banca ética|Banco malo|Caixa de aforros|Caixeiro automático)$",
                        validCategoryPattern = "^(Bancos|Caixas)",
                        invalidCategoryAsPagePattern = "^(Personalidades)",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/economía/empresas/telecomunicacións.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryOfPagesNames = ["Empresas de telecomunicacións"],
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/historia/acontecementos.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Acontecementos históricos"],
                        validCategoryPattern = "^(Atentados|Golpes de estado)",
                        invalidCategoryAsPagePattern = "^(Catástrofes|Conflitos|Declaracións de independencia|Guerras|Revolucións)", # Ignorados, polos menos de momento.
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/historia/civilizacións.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Civilizacións"],
                        invalidPagePattern = "^((Lista|Pobos) |(Civilización|Cultura dos Campos de Urnas|Sala do hidromel)$)",
                        validCategoryPattern = "^(Pobos|Reinos) ",
                        invalidCategoryAsPagePattern = "^(Arquitectura|Xeografía) ",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    pattern = "Linguaxes"
    generators.append(GalipediaGenerator(
        resource = "onomástica/informática/linguaxes.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Linguaxes informáticas"],
                        invalidPagePattern = "^(Imaxes |Listas |Exemplos |Linguaxe compilada|Linguaxe de alto nivel|Linguaxe de programación$|Linguaxe interpretada|{})".format(pattern),
                        validCategoryPattern = "^({})".format(pattern),
                    ),
                ],
                pageParser = FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/informática/sistemas-de-almacenamento.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Sistemas de almacenamento"],
                        invalidPagePattern = "^Administrador de base de datos$",
                    ),
                ],
                pageParser = FirstSentenceParser(),
            )
        ],
    ))

    pattern = "Redes sociais|Weblogs|Wikipedias|Xornais"
    generators.append(GalipediaGenerator(
        resource = "onomástica/informática/sitios-de-internet.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Sitios de internet"],
                        invalidPagePattern = "^(Imaxes |Listas |(Rede social|Blogueiros)$|{})".format(pattern),
                        validCategoryPattern = "^({})".format(pattern),
                        invalidCategoryAsPagePattern = "^Blogueiros$"
                    ),
                ],
                pageParser = FirstSentenceParser(),
            )
        ],
    ))

    pattern = "Ambientes de escritorio|Aplicacións|Compañías|Conxuntos de aplicacións|Editores|Emuladores|Follas|Kernels|Navegadores|Personaxes|Procesadores|Reprodutores|Servidores|Sistemas operativos|Software|Utilidades|Videoxogos|Xestores"
    generators.append(GalipediaGenerator(
        resource = "onomástica/informática/software.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Software móbil", "Software por tipo"],
                        invalidPagePattern = "^(Imaxes |Listas |Premio |Navegador web|Videoxogo|{})".format(pattern),
                        validCategoryPattern = "^({})".format(pattern),
                    ),
                ],
                pageParser = FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/música/acontecementos.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Acontecementos musicais"],
                        validCategoryPattern = "^(Acontecementos|Festivais|Xiras)",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/organizacións/asociacións.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Asociacións"],
                        invalidPagePattern = "^(Colectivo|Gremio|Organización non gobernamental)$",
                        validCategoryPattern = "^(Asociacións|Bandas|Centros|Corais|Entidades|Fundacións|Organizacións|Orquestras|Sindicatos)",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/organizacións/deporte.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Organizacións deportivas"],
                        validCategoryPattern = "^(Autoridades|Federacións|Organismos|Organizacións)",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/organizacións/economía.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Organizacións económicas"],
                        validCategoryPattern = "^Organizacións",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/organizacións/internacionais.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Organizacións internacionais"],
                        invalidPagePattern = "^Organización Internacional$",
                        validCategoryPattern = "^Organizacións",
                        invalidCategoryAsPagePattern = "^Personalidades",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/política/partidos.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Partidos políticos"],
                        validCategoryPattern = "^(Partidos )",
                        invalidPagePattern = "^(Lista|Partidos) ",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            ),
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Listas de política"],
                        validPagePattern = "^Partidos inscritos",
                    ),
                ],
                pageParser=TableParser(cellNumbers=[0, 1], skipRows=[0,]),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/televisión/cadeas.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Cadeas de televisión"],
                        validCategoryPattern = "^(Cadeas|Televisións)",
                        invalidCategoryAsPagePattern = "^Programas",
                        invalidPagePattern = "^(Cadea de televisión|Televisión comunitaria)$",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/televisión/debuxos-animados.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Debuxos animados"],
                        validCategoryPattern = "^(Anime$|Personaxes)",
                        invalidPagePattern = "^Anime$",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/televisión/grupos.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Grupos de televisión de España"],
                        invalidCategoryPattern = ".",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/toponimia/accidentes/baías.dic",
        partOfSpeech = "topónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Golfos e baías"],
                        invalidPagePattern = "^Baía$",
                        validCategoryPattern = "^Golfos e baías d",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/toponimia/accidentes/desertos.dic",
        partOfSpeech = "topónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Desertos"],
                        invalidPagePattern = "^(Desertos d|(Deserto|Serir)$)",
                        validCategoryPattern = "^Desertos d",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/toponimia/accidentes/illas.dic",
        partOfSpeech = "topónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryOfPagesNames = [
                            "Illas e arquipélagos",
                            "Arquipélagos",
                            "Atois",
                            "Illas",
                            "Illas da Bretaña",
                            "Illas das Illas Baleares",
                            "Illas de Asturias",
                            "Illas de Canarias",
                            "Illas de Cataluña",
                            "Illas de Escocia",
                            "Illas de Galicia",
                            "Illas deshabitadas",
                            "Illas ficticias",
                            "Illas galegas",
                            "Illas dos Grandes Lagos"
                        ],
                        invalidPagePattern = "^((Batalla|Lista) |Illa deserta|Illote Motu|Illas de Galicia)",
                        invalidCategoryAsPagePattern = "^(Illas da baía d.*|Illas do arquipélago d.*|Illas do Xapón|Illas e arquipélagos .*)$",
                        categoryOfCategoriesNames = ["Illas e arquipélagos por localización", "Illas por continente", "Illas por mar", "Illas por países"],
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/toponimia/accidentes/mares.dic",
        partOfSpeech = "topónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Mares e océanos"],
                        invalidPagePattern = "^(Instituto|(Mar|Océano mundial|Zona económica exclusiva)$)",
                        validCategoryPattern = "^(Mares|Océanos)",
                        invalidCategoryAsPagePattern = "^(Cidades|Estreitos) ",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/toponimia/accidentes/montañas.dic",
        partOfSpeech = "topónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Montañas"],
                        validCategoryPattern = "^(Cordilleiras|Cumes|Montañas|Montes)",
                        invalidPagePattern = "^(Cumes máis|Lista)",
                        invalidCategoryAsPagePattern = "^(Imaxes) ",
                    ),
                ],
            ),
            EntryGenerator(
                pageGenerators = [
                    PageLoader(pageNames=["Montes de Galicia",]),
                ],
                pageParser=TableParser(cellNumbers=[0, 2], skipRows=[0,]),
                entryParser=EntryParser(separatorsSplitter=["/",]),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/toponimia/accidentes/penínsulas.dic",
        partOfSpeech = "topónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Penínsulas"],
                        validCategoryPattern = "^Penínsulas (a|d|por |n)",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    pattern = "(Praias) "
    generators.append(GalipediaGenerator(
        resource = "onomástica/toponimia/accidentes/praias.dic",
        partOfSpeech = "topónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Praias"],
                        invalidPagePattern = "^({pattern}|Bandeira Azul$|Galería de imaxes|Praia$|Praia nudista$)".format(pattern=pattern),
                        validCategoryPattern = "^{pattern}".format(pattern=pattern),
                        invalidCategoryAsPagePattern = "^(Imaxes) ",
                    ),
                ],
            )
        ],
    ))

    pattern = "(Rexións) "
    generators.append(GalipediaGenerator(
        resource = "onomástica/toponimia/accidentes/rexións.dic",
        partOfSpeech = "topónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    PageLoader(
                        [
                            "Cisxordania",
                            "Cochinchina",
                            "Dalmacia",
                            "Faixa de Gaza",
                        ]
                    ),
                    CategoryBrowser(
                        categoryNames = ["Rexións de Europa"],
                        invalidPagePattern = "^({pattern}|Galería de imaxes)".format(pattern=pattern),
                        validCategoryPattern = "^{pattern}".format(pattern=pattern),
                        invalidCategoryAsPagePattern = "^(Imaxes) ",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    pattern = "(Afluentes|Regatos|Ríos) "
    generators.append(GalipediaGenerator(
        resource = "onomástica/toponimia/accidentes/ríos.dic",
        partOfSpeech = "topónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        invalidPagePattern = "^({pattern}|(Galería de imaxes|Hidrografía|Lista) |(Caneiro \(muíño\)|Pasadoiro|Pontella \(pasaxe\))$)".format(pattern=pattern),
                        validCategoryPattern = "^{pattern}".format(pattern=pattern),
                        invalidCategoryAsPagePattern = "^({pattern}|Imaxes)".format(pattern=pattern),
                        categoryOfCategoriesNames = ["Ríos"],
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaLocalidadesGenerator("Desaparecidas", ["Cidades desaparecidas"])) # Localidades desaparecidas.
    generators.append(GalipediaLocalidadesGenerator("Alemaña", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Alxeria"))
    generators.append(GalipediaLocalidadesGenerator("Austria", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Bangladesh"))
    generators.append(GalipediaLocalidadesGenerator("Barbados"))
    generators.append(GalipediaLocalidadesGenerator("Bélxica", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Bolivia"))
    generators.append(GalipediaLocalidadesGenerator("Brasil", ["Cidades do {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Burkina Faso", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Cambodja", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Canadá", ["Cidades do {name}"], pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("China", ["Cidades da {name}"], pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Colombia", ["Cidades de {name}", "Concellos de {name}", "Correxementos de {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Congo", ["Cidades da República do {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Corea do Norte", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Cuba"))
    generators.append(GalipediaLocalidadesGenerator("Dinamarca"))
    generators.append(GalipediaLocalidadesGenerator("Dominica", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Emiratos Árabes Unidos", ["Cidades dos {name}"], pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Eslovaquia"))
    generators.append(GalipediaLocalidadesGenerator("España", ["Cidades autónomas de {name}", "Cidades de {name}", "Concellos de {name}", "Parroquias de España"], pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Estados Unidos de América", ["Cidades dos {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Etiopía"))
    generators.append(GalipediaLocalidadesGenerator("Exipto"))
    generators.append(GalipediaLocalidadesGenerator("Finlandia", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Francia", ["Cidades de {name}", "Concellos de {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Grecia", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Grecia antiga", ["Antigas cidades gregas"]))
    generators.append(GalipediaLocalidadesGenerator("Guatemala", ["Cidades de {name}", "Municipios de {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Guinea-Bisau"))
    generators.append(GalipediaLocalidadesGenerator("Hungría"))
    generators.append(GalipediaLocalidadesGenerator("Iemen"))
    generators.append(GalipediaLocalidadesGenerator("India", ["Cidades da {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Indonesia"))
    generators.append(GalipediaLocalidadesGenerator("Iraq"))
    generators.append(GalipediaLocalidadesGenerator("Irlanda"))
    generators.append(GalipediaLocalidadesGenerator("Islandia", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Israel"))
    generators.append(GalipediaLocalidadesGenerator("Italia", ["Cidades de {name}", "Concellos de {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Kenya", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Líbano", ["Cidades do {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Malaisia"))
    generators.append(GalipediaLocalidadesGenerator("Malí"))
    generators.append(GalipediaLocalidadesGenerator("Marrocos", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("México", ["Cidades de {name}", "Cidades prehispánicas de {name}", "Concellos de {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Mozambique", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Nepal", ["Cidades do {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Oceanía"))
    generators.append(GalipediaLocalidadesGenerator("Omán", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Países Baixos", ["Cidades dos {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Paquistán"))
    generators.append(GalipediaLocalidadesGenerator("Perú", ["Cidades do {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Polonia"))
    generators.append(GalipediaLocalidadesGenerator("Portugal", ["Cidades de {name}", "Municipios de {name}", "Vilas de {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Qatar"))
    generators.append(GalipediaLocalidadesGenerator("Reino Unido", ["Cidades do {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Romanía"))
    generators.append(GalipediaLocalidadesGenerator("Rusia", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Serbia"))
    generators.append(GalipediaLocalidadesGenerator("Siria"))
    generators.append(GalipediaLocalidadesGenerator("Sudán do Sur", ["Localidades de {name}"]))
    generators.append(GalipediaLocalidadesGenerator("Suecia"))
    generators.append(GalipediaLocalidadesGenerator("Suráfrica"))
    generators.append(GalipediaLocalidadesGenerator("Suíza"))
    generators.append(GalipediaLocalidadesGenerator("Timor Leste"))
    generators.append(GalipediaLocalidadesGenerator("Turquía"))
    generators.append(GalipediaLocalidadesGenerator("Ucraína", pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Venezuela"))
    generators.append(GalipediaLocalidadesGenerator("Xapón", ["Concellos do {name}"], pageParser=FirstSentenceParser()))
    generators.append(GalipediaLocalidadesGenerator("Xordania"))

    generators.append(GalipediaGenerator(
        resource = "onomástica/toponimia/lugares/galicia.dic",
        partOfSpeech = "topónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Lugares de Galicia", "Parroquias de Galicia"],
                        invalidPagePattern = "^(Lugares d|Parroquias d)",
                        validCategoryPattern = "^(Lugares d|Parroquias d)",
                        invalidCategoryAsPagePattern = "(^Imaxes d.*|.* sen imaxes$)"
                    ),
                ],
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/toponimia/países.dic",
        partOfSpeech = "topónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = [
                            "Estados desaparecidos",
                            "Países con recoñecemento limitado",
                            "Países de América",
                            "Países de Asia",
                            "Países de Europa",
                            "Países de Oceanía",
                            "Países de África"
                        ],
                        invalidPagePattern = "^(Concellos |Galería d|Historia d|Lista d|Principais cidades )",
                        validCategoryPattern = "^(Estados desaparecidos d|Imperios|Países d)",
                        invalidCategoryAsPagePattern = "^(Capitais d|Emperadores$)",
                    ),
                ],
                pageParser=FirstSentenceParser(),
                entryParser=EntryParser(commaFilter=False),
            )
        ],
    ))

    generators.append(GalipediaRexionsGenerator("Alemaña", ["Estados de {name}", "Rexións de {name}"]))
    generators.append(GalipediaRexionsGenerator("Bélxica", ["Provincias da {name}", "Rexións de {name}"], pageParser=FirstSentenceParser()))
    generators.append(GalipediaRexionsGenerator("Brasil", ["Estados do {name}"]))
    generators.append(GalipediaRexionsGenerator("Chile"))
    generators.append(GalipediaRexionsGenerator("Colombia", ["Departamentos de {name}", "Provincias de {name}"]))
    generators.append(GalipediaRexionsGenerator("España", ["Comarcas de {name}", "Comunidades autónomas de {name}", "Provincias de {name}"]))
    generators.append(GalipediaRexionsGenerator("Estados Unidos de América", ["Estados dos {name}", "Distritos de Nova York"]))
    generators.append(GalipediaRexionsGenerator("Finlandia"))
    generators.append(GalipediaRexionsGenerator("Francia", ["Departamentos de {name}"]))
    generators.append(GalipediaRexionsGenerator("Grecia", ["Periferias de {name}"]))
    generators.append(GalipediaRexionsGenerator("Guatemala", ["Departamentos de {name}"]))
    generators.append(GalipediaRexionsGenerator("India", ["Subdivisións da {name}"]))
    generators.append(GalipediaRexionsGenerator("Italia", ["Rexións de {name}", "Provincias de {name}"]))
    generators.append(GalipediaRexionsGenerator("México", ["Estados de {name}"]))
    generators.append(GalipediaRexionsGenerator("Países Baixos", ["Provincias dos {name}"]))
    generators.append(GalipediaRexionsGenerator("Portugal", [
        "Antigas provincias de {name}",
        "Distritos de {name}",
        "NUTS I portuguesas",
        "NUTS II portuguesas",
        "NUTS III portuguesas",
        "Rexións autónomas de {name}",
    ]))
    generators.append(GalipediaRexionsGenerator("Reino Unido", ["Condados de Inglaterra", "Condados de Irlanda", "Divisións de Escocia", "Rexións de Inglaterra"]))
    generators.append(GalipediaRexionsGenerator("Rusia", ["Repúblicas de {name}"]))

    generators.append(GalipediaGenerator(
        resource = "onomástica/toponimia/zonas/españa.dic",
        partOfSpeech = "topónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Barrios de España", "Distritos de España"],
                        invalidPagePattern = "^(Barrios|Distritos) ",
                        validCategoryPattern = "^(Barrios|Distritos) "
                    ),
                ],
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/toponimia/zonas/mónaco.dic",
        partOfSpeech = "topónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Barrios de Mónaco"],
                        invalidPagePattern = "^Barrios ",
                        validCategoryPattern = "^Barrios "
                    ),
                ],
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/transporte/aeroportos.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    PageLoader(pageNames=["Lista de aeroportos de España",]),
                ],
                pageParser=TableParser(cellNumbers=[4, 5], skipRows=[0,]),
                entryParser=EntryParser(ignoredEntries=["?", "-"]),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/transporte/barcos.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Barcos"],
                        invalidPagePattern = "^(Babor|Barco|Calado|Desprazamento \(náutica\)|Estribor|Francobordo|Navegación marítima|Popa|Proa|Puntal)$",
                        validCategoryPattern = "^(Barcos|Embarcacións)",
                        invalidCategoryAsPagePattern = "^(Accidentes|Imaxes|Tipos)",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/transporte/liñas-aéreas.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Aeroliñas"],
                        invalidPagePattern = "^(Aeroliña|Aeroliña de baixo custo)$",
                        validCategoryPattern = "^Aeroliñas",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "onomástica/transporte/trens.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Trens"],
                        validCategoryPattern = "^Trens",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))


    lineExpression = "^\* *(\'\'\')? *(\[\[)? *([^][|\']+\|)? *(?P<entry>[^][|\']+) *(\]\])? *(\'\'\')? *:"

    generators.append(GalipediaGenerator(
        resource = "siglas/campos/alimentación.dic",
        partOfSpeech = "sigla",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    PageLoader(pageNames = ["Lista de siglas e acrónimos de alimentación",],),
                ],
                pageParser=LineParser(lineExpression),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "siglas/campos/informática.dic",
        partOfSpeech = "sigla",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    PageLoader(pageNames = ["Lista de siglas e acrónimos de informática",],),
                ],
                pageParser=LineParser(lineExpression),
            ),
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Acrónimos de informática"],
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "siglas/campos/medicina.dic",
        partOfSpeech = "sigla",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    PageLoader(pageNames = ["Lista de acrónimos en Medicina",],),
                ],
                pageParser=LineParser(lineExpression),
            )
        ],
    ))

    generators.append(GalipediaGenerator(
        resource = "siglas/xeral.dic",
        partOfSpeech = "sigla",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    PageLoader(pageNames = ["Lista de siglas e acrónimos",],),
                ],
                pageParser=LineParser(lineExpression),
            ),
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Acrónimos"],
                        invalidCategoryPattern = ".",
                        invalidCategoryAsPagePattern = ".",
                        invalidPagePattern = "^(Acrónimo recursivo$|Lista )",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))


    # Wikipedia en castelán.

    generators.append(WikipediaEsGenerator(
        resource = "antroponimia/xeral.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Nombres por género"],
                        validCategoryPattern = "^Nombres ",
                        invalidCategoryAsPagePattern = "^Puranas$",
                    ),
                ],
            )
        ],
    ))

    generators.append(WikipediaEsGenerator(
        resource = "antroponimia/países/españa.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = [
                            "Ciclistas de la Comunidad de Madrid",
                        ],
                    ),
                ],
            )
        ],
    ))

    generators.append(WikipediaEsGenerator(
        resource = "antroponimia/países/italia.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = [
                            "Botánicos de Italia del siglo XV",
                            "Religiosos de Italia del siglo XV",
                            "Santos de Italia",
                        ],
                        validCategoryPattern = "^(Cardenales|Obispos|Santos)",
                    ),
                ],
            )
        ],
    ))

    generators.append(WikipediaEsGenerator(
        resource = "empresas/seguros.dic",
        partOfSpeech = "nome",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Aseguradoras"],
                        invalidPagePattern = "^(Compañía de seguros|Reaseguro|Sector asegurador en España)$",
                        validCategoryPattern = "^(Aseguradoras|Reaseguradoras)",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))


    # Wikipedia en inglés.

    generators.append(WikipediaEnGenerator(
        resource = "antroponimia/xeral.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Given names by gender"],
                        validCategoryPattern = ".* (god(desse)?s|names)",
                        invalidPagePattern = "^(Consorts of Ganesha|Forms of Parvati|Ganges in Hinduism|.* Temple)$",
                    ),
                ],
            ),
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Basque given names"],
                        validCategoryPattern = ".* names",
                    ),
                ],
                pageParser=FirstSentenceParser(),
            )
        ],
    ))

    generators.append(WikipediaEnGenerator(
        resource = "antroponimia/países/brasil.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = [
                            "People by state in Brazil",
                            "Brazilian mixed martial artists",
                        ],
                        validCategoryPattern = ".*",
                    ),
                ],
            )
        ],
    ))

    generators.append(WikipediaEnGenerator(
        resource = "antroponimia/países/italia.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Italian sculptors"],
                        validCategoryPattern = ".* (sculptors|stubs)",
                    ),
                ],
            )
        ],
    ))

    generators.append(WikipediaEnGenerator(
        resource = "antroponimia/países/méxico.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["People from Oaxaca"],
                        validCategoryPattern = "^(Members|Musicians|People|Singers) ",
                    ),
                ],
            )
        ],
    ))

    generators.append(WikipediaEnGenerator(
        resource = "antroponimia/países/países-baixos.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Dutch footballers"],
                        invalidPagePattern = "^List of",
                        validCategoryPattern = ".*(footballers|stubs)",
                    ),
                ],
            )
        ],
    ))

    generators.append(WikipediaEnGenerator(
        resource = "antroponimia/países/rusia.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["People from Bryansk"],
                    ),
                ],
            )
        ],
    ))

    # Wikipedia en húngaro.

    generators.append(WikipediaHuGenerator(
        resource = "antroponimia.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryOfCategoriesNames = ["Férfikeresztnevek", "Női keresztnevek"],
                    ),
                ],
            )
        ],
    ))

    # Wikipedia en portugués.

    generators.append(WikipediaPtGenerator(
        resource = "antroponimia/países/brasil.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Atores do Brasil",],
                        invalidPagePattern = "^(Anexo:|Vedete$)",
                        validCategoryPattern = "^(Atores|Estrelas|Vedetes)",
                        invalidCategoryAsPagePattern = "^(!|Imagens)",
                    ),
                ],
            )
        ],
    ))

    generators.append(WikipediaPtGenerator(
        resource = "antroponimia/países/españa.dic",
        partOfSpeech = "antropónimo",
        entryGenerators=[
            EntryGenerator(
                pageGenerators = [
                    CategoryBrowser(
                        categoryNames = ["Reis de Leão",],
                        invalidPagePattern = "^Anexo:",
                    ),
                ],
            )
        ],
    ))


    return generators